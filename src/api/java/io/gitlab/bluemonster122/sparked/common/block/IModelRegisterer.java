package io.gitlab.bluemonster122.sparked.common.block;

import io.gitlab.bluemonster122.sparked.Sparked;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.apache.logging.log4j.Level;

/**
 * For use on Item and Block classes only.
 * <br>
 * Created by <a href="https://www.github.com/bluemonster122">BlueMonster122</a> on 15/07/2018
 */
public interface IModelRegisterer {

    @SideOnly(Side.CLIENT)
    default void initModel() {
        Item item;
        if (this instanceof Block) {
            item = Item.getItemFromBlock((Block)this);
        } else if (this instanceof Item) {
            item = (Item) this;
        } else {
            Sparked.logger.log(Level.ERROR, "Tried to populate Model for an object that is neither a Block or Item.", new IllegalStateException("IModelRegisterer was implemented in a non-Block, non-Item class."));
            return;
        }
        ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(item.getRegistryName(), "inventory"));
    }
}

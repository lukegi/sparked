package io.gitlab.bluemonster122.sparked.common.config;

import io.gitlab.bluemonster122.sparked.common.reference.ModInfo;
import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.Config.LangKey;

@Config(modid = ModInfo.MOD_ID, category = "rewards")
public class SConfigRewards {

    //    @Comment("Set to false to stop advancements dropping rewards")
    @LangKey("sparked.config.rewards.advancement")
    public static boolean giveAdvancementReward = true;
}

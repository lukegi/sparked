package io.gitlab.bluemonster122.sparked.common;

import io.gitlab.bluemonster122.sparked.common.block.BlockMagicalLog;
import io.gitlab.bluemonster122.sparked.common.block.SBlock;
import io.gitlab.bluemonster122.sparked.common.reference.ModInfo;
import net.minecraftforge.fml.common.registry.GameRegistry.ObjectHolder;

@ObjectHolder(ModInfo.MOD_ID)
public class SBlocks {
    /*
     * WOOD
     */
    public static final BlockMagicalLog LOG_MAGICAL_REGULAR = null;
    public static final BlockMagicalLog LOG_MAGICAL_COLORLESS = null;
    public static final BlockMagicalLog LOG_MAGICAL_WHITE = null;
    public static final BlockMagicalLog LOG_MAGICAL_BLUE = null;
    public static final BlockMagicalLog LOG_MAGICAL_BLACK = null;
    public static final BlockMagicalLog LOG_MAGICAL_RED = null;
    public static final BlockMagicalLog LOG_MAGICAL_GREEN = null;
    public static final SBlock PLANKS_MAGICAL = null;
}

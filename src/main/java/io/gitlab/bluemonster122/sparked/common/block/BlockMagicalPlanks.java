package io.gitlab.bluemonster122.sparked.common.block;

import net.minecraft.block.material.Material;

public class BlockMagicalPlanks extends SBlock {
    public BlockMagicalPlanks() {
        super(Material.WOOD);
    }
}

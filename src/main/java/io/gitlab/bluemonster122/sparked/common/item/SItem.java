package io.gitlab.bluemonster122.sparked.common.item;

import io.gitlab.bluemonster122.sparked.common.STab;
import io.gitlab.bluemonster122.sparked.common.block.IModelRegisterer;
import net.minecraft.item.Item;

public class SItem extends Item implements IModelRegisterer {

    public SItem() {
        this.setCreativeTab(STab.INSTANCE);
    }
}

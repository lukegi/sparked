package io.gitlab.bluemonster122.sparked.common.magic;

import net.minecraft.util.IStringSerializable;

public enum Color implements IStringSerializable {
    COLORLESS(0), WHITE(1), BLUE(2), BLACK(3), RED(4), GREEN(5);

    private int meta;

    Color(int meta) {
        this.meta = meta;
    }

    public static Color fromMeta(int meta) {
        switch (meta) {
            case 1:
                return WHITE;
            case 2:
                return BLUE;
            case 3:
                return BLACK;
            case 4:
                return RED;
            case 5:
                return GREEN;
            case 0:
            default:
                return COLORLESS;
        }
    }

    @Override
    public String getName() {
        return name().toLowerCase();
    }

    public int getMeta() {
        return meta;
    }
}

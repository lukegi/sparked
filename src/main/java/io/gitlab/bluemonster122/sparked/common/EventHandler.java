package io.gitlab.bluemonster122.sparked.common;

import io.gitlab.bluemonster122.sparked.Sparked;
import io.gitlab.bluemonster122.sparked.common.config.SConfigRewards;
import io.gitlab.bluemonster122.sparked.common.entity.EntityGoblin;
import io.gitlab.bluemonster122.sparked.common.multiblock.SMultiblocks;
import io.gitlab.bluemonster122.sparked.common.reference.ModInfo;
import io.gitlab.bluemonster122.sparked.common.reference.Names;
import io.gitlab.bluemonster122.sparked.common.util.StackUtils;
import net.minecraft.advancements.Advancement;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.projectile.EntityEgg;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.RayTraceResult.Type;
import net.minecraft.world.World;
import net.minecraft.world.storage.WorldInfo;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.ProjectileImpactEvent;
import net.minecraftforge.event.entity.player.AdvancementEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.Event.Result;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;

@EventBusSubscriber(modid = ModInfo.MOD_ID)
public class EventHandler {

    @SubscribeEvent
    public static void onPlayerJoinWorld(EntityJoinWorldEvent event) {

    }

    @SubscribeEvent
    public static void onPlayerLogin(PlayerEvent.PlayerLoggedInEvent event) {
        if (event.player instanceof EntityPlayerMP) {
            EntityPlayerMP playerMP = (EntityPlayerMP) event.player;
            Advancement advancement = playerMP.getServerWorld().getAdvancementManager().getAdvancement(Names.PROGRESSION_ROOT);
            playerMP.getAdvancements().grantCriterion(advancement, "impossible");
        }
    }

    @SubscribeEvent
    public static void onEggImpact(ProjectileImpactEvent.Throwable event) {
        EntityThrowable entity = event.getThrowable();
        if (entity.getEntityWorld().isRemote) {
            return;
        }
        RayTraceResult rayTraceResult = event.getRayTraceResult();
        if (entity instanceof EntityEgg && rayTraceResult.typeOfHit == Type.BLOCK && entity.getThrower() instanceof EntityPlayerMP) {
            // If player has already killed a golbin, they can't summon another.
            EntityPlayerMP playerMP = (EntityPlayerMP) entity.getThrower();
            Advancement ignition = playerMP.getServerWorld().getAdvancementManager().getAdvancement(Names.PROGRESSION_SPARK);
            if (playerMP.getAdvancements().getProgress(ignition).isDone()) {
                return;
            }
            World world = entity.getEntityWorld();
            BlockPos base = rayTraceResult.getBlockPos().offset(rayTraceResult.sideHit);
            if (SMultiblocks.GOBLIN_MULTIBLOCK.checkMultiblock(world, base) && SMultiblocks.GOBLIN_MULTIBLOCK.clearMultiblockBlocks(world, base)) {
                Sparked.logger.info("SUCCESS");
                world.addWeatherEffect(new EntityLightningBolt(world, base.getX() + 0.5, base.getY() + 0.5, base.getZ() + 0.5, true));
                makeItRain(world, 200 + world.rand.nextInt(12000));
                entity.setDead();
                world.spawnEntity(new EntityGoblin(world, rayTraceResult.hitVec));
                event.setResult(Result.DENY);
                event.setCanceled(true);
            }
        }
    }

    private static void makeItRain(World world, int stormDuration) {
        WorldInfo worldInfo = world.getWorldInfo();
        worldInfo.setCleanWeatherTime(0);
        worldInfo.setRainTime(stormDuration);
        worldInfo.setThunderTime(stormDuration);
        worldInfo.setRaining(true);
        worldInfo.setThundering(true);
    }

    @SubscribeEvent
    public static void onAdvancementMade(AdvancementEvent event) {
        ResourceLocation id = event.getAdvancement().getId();
        EntityPlayer player = event.getEntityPlayer();
        if (Names.PROGRESSION_SPARK.equals(id)) {
            StackUtils.giveItemToPlayer(player, new ItemStack(SItems.SPELLBOOK, 1));
        }

        if (SConfigRewards.giveAdvancementReward) {
            if (Names.PROGRESSION_SPELLBOOK.equals(id)) {
                StackUtils.giveItemToPlayer(player, new ItemStack(SItems.STICK_MAGICAL, 8));
            }
        }
    }
}

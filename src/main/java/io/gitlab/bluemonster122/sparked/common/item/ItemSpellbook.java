package io.gitlab.bluemonster122.sparked.common.item;

import io.gitlab.bluemonster122.sparked.Sparked;
import io.gitlab.bluemonster122.sparked.common.advancement.trigger.SpellbookUseTrigger;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ItemSpellbook extends SItem {

    public ItemSpellbook() {
        super();
        this.setMaxStackSize(1);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
        if (playerIn instanceof EntityPlayerMP) {
            SpellbookUseTrigger.SPELLBOOK_USE_TRIGGER.trigger((EntityPlayerMP) playerIn, this);
        }
        playerIn.openGui(Sparked.INSTANCE, 0, worldIn, (int) playerIn.posX, (int) playerIn.posY, (int) playerIn.posZ);
        return ActionResult.newResult(EnumActionResult.SUCCESS, playerIn.getHeldItem(handIn));
    }
}

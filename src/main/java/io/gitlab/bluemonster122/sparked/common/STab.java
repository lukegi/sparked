package io.gitlab.bluemonster122.sparked.common;

import io.gitlab.bluemonster122.sparked.common.reference.ModInfo;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;

public final class STab {

    public static final CreativeTabs INSTANCE = new CreativeTabs(ModInfo.RESOURCE_PREFIX + "main") {
        @Override
        public ItemStack getTabIconItem() {
            return new ItemStack(SItems.PWSPARK);
        }

        @Override
        public void displayAllRelevantItems(NonNullList<ItemStack> p_78018_1_) {
            p_78018_1_.add(new ItemStack(SItems.SPELLBOOK));
            p_78018_1_.add(new ItemStack(SItems.MINING_TOOL));
            p_78018_1_.add(new ItemStack(SItems.PWSPARK));
            p_78018_1_.add(ItemStack.EMPTY);
            p_78018_1_.add(ItemStack.EMPTY);
            p_78018_1_.add(ItemStack.EMPTY);
            p_78018_1_.add(ItemStack.EMPTY);
            p_78018_1_.add(ItemStack.EMPTY);
            p_78018_1_.add(ItemStack.EMPTY);
            p_78018_1_.add(new ItemStack(SBlocks.LOG_MAGICAL_REGULAR));
            p_78018_1_.add(new ItemStack(SBlocks.LOG_MAGICAL_COLORLESS));
            p_78018_1_.add(new ItemStack(SBlocks.LOG_MAGICAL_WHITE));
            p_78018_1_.add(new ItemStack(SBlocks.LOG_MAGICAL_BLUE));
            p_78018_1_.add(new ItemStack(SBlocks.LOG_MAGICAL_BLACK));
            p_78018_1_.add(new ItemStack(SBlocks.LOG_MAGICAL_RED));
            p_78018_1_.add(new ItemStack(SBlocks.LOG_MAGICAL_GREEN));
            p_78018_1_.add(new ItemStack(SBlocks.PLANKS_MAGICAL));
            p_78018_1_.add(new ItemStack(SItems.STICK_MAGICAL));
        }
    };
}

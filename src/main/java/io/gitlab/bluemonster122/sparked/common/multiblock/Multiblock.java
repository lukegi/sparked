package io.gitlab.bluemonster122.sparked.common.multiblock;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;

import java.util.Map.Entry;
import java.util.function.Predicate;

public class Multiblock {

    private final String ID;
    private HashMultimap<Vec3i, Predicate<IBlockState>> form = HashMultimap.create();
    private Predicate<IBlockState> originPredicate;

    public Multiblock(String ID) {
        this.ID = ID;
    }

    public boolean setForm(ImmutableList<Vec3i> positions, ImmutableList<Predicate<IBlockState>> values) {
        if (positions.size() != values.size()) {
            return false;
        }
        for (int i = 0; i < positions.size(); i++) {
            form.put(positions.get(i), values.get(i));
            if (positions.get(i).equals(Vec3i.NULL_VECTOR)) {
                originPredicate = values.get(i);
            }
        }
        return true;
    }

    /**
     * Always call {@link #checkMultiblock(World, BlockPos)} before calling this!
     *
     * @param world  World to clear the multiblock from
     * @param origin Where in the world the origin is.
     * @return true if completely successful.
     */
    public boolean clearMultiblockBlocks(World world, BlockPos origin) {
        for (Vec3i vec3i : form.keys()) {
            if (!world.setBlockToAir(origin.add(vec3i))) {
                return false;
            }
        }
        return true;
    }

    public boolean checkMultiblock(World world, BlockPos origin) {
        for (Entry<Vec3i, Predicate<IBlockState>> entry : form.entries()) {
            if (!entry.getValue().test(world.getBlockState(origin.add(entry.getKey())))) {
                return false;
            }
        }
        return true;
    }

    public Predicate<IBlockState> getOrigin() {
        return originPredicate;
    }

    public void setOrigin(Predicate<IBlockState> origin) {
        this.originPredicate = origin;
    }

    public String getID() {
        return ID;
    }
}

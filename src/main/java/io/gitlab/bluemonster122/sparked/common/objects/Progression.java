package io.gitlab.bluemonster122.sparked.common.objects;

import io.gitlab.bluemonster122.sparked.Sparked;
import io.gitlab.bluemonster122.sparked.client.render.RenderGoblin;
import io.gitlab.bluemonster122.sparked.common.SItems;
import io.gitlab.bluemonster122.sparked.common.advancement.trigger.SpellbookUseTrigger;
import io.gitlab.bluemonster122.sparked.common.entity.EntityGoblin;
import io.gitlab.bluemonster122.sparked.common.item.ItemSpellbook;
import io.gitlab.bluemonster122.sparked.common.item.SItem;
import io.gitlab.bluemonster122.sparked.common.reference.Names;
import io.gitlab.bluemonster122.sparked.common.util.RegistryObject;
import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.world.storage.loot.LootTableList;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.registry.EntityRegistry;

public class Progression extends RegistryObject {
    public Progression() {
        super("progression");
    }

    @Override
    public void registerBlocks() {
        /* NOOP */
    }

    @Override
    public void registerItems() {
        registerItem(new SItem(), Names.PWSPARK);
        registerItem(new ItemSpellbook(), Names.SPELLBOOK);
    }

    @Override
    public void registerModels() {
        SItems.PWSPARK.initModel();
        SItems.SPELLBOOK.initModel();
        RenderingRegistry.registerEntityRenderingHandler(EntityGoblin.class, RenderGoblin::new);
    }

    @Override
    public void registerOreDict() {
        /* NOOP */
    }

    @Override
    public void registerEntities() {
        EntityRegistry.registerModEntity(Names.GOBLIN, EntityGoblin.class, "goblin", 0, Sparked.INSTANCE, 32, 1, true);
        LootTableList.register(EntityGoblin.GOBLIN_LOOT);
    }

    @Override
    public void registerAdvancements() {
        SpellbookUseTrigger.SPELLBOOK_USE_TRIGGER = CriteriaTriggers.register(new SpellbookUseTrigger());
    }
}

package io.gitlab.bluemonster122.sparked.common.objects;

import io.gitlab.bluemonster122.sparked.common.SBlocks;
import io.gitlab.bluemonster122.sparked.common.SItems;
import io.gitlab.bluemonster122.sparked.common.block.BlockMagicalLog;
import io.gitlab.bluemonster122.sparked.common.block.SBlock;
import io.gitlab.bluemonster122.sparked.common.block.property.EnumWoodType;
import io.gitlab.bluemonster122.sparked.common.item.SItem;
import io.gitlab.bluemonster122.sparked.common.item.block.SItemBlock;
import io.gitlab.bluemonster122.sparked.common.reference.Names;
import io.gitlab.bluemonster122.sparked.common.util.RegistryObject;
import net.minecraft.block.material.Material;
import net.minecraftforge.oredict.OreDictionary;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class Wood extends RegistryObject {
    public Wood() {
        super("wood");
    }

    @Override
    public void registerBlocks() {
        registerBlock(new BlockMagicalLog(EnumWoodType.REGULAR), Names.LOG_MAGICAL_REGULAR);
        registerBlock(new BlockMagicalLog(EnumWoodType.COLORLESS), Names.LOG_MAGICAL_COLORLESS);
        registerBlock(new BlockMagicalLog(EnumWoodType.WHITE), Names.LOG_MAGICAL_WHITE);
        registerBlock(new BlockMagicalLog(EnumWoodType.BLUE), Names.LOG_MAGICAL_BLUE);
        registerBlock(new BlockMagicalLog(EnumWoodType.BLACK), Names.LOG_MAGICAL_BLACK);
        registerBlock(new BlockMagicalLog(EnumWoodType.RED), Names.LOG_MAGICAL_RED);
        registerBlock(new BlockMagicalLog(EnumWoodType.GREEN), Names.LOG_MAGICAL_GREEN);
        registerBlock(new SBlock(Material.WOOD), Names.PLANKS_MAGICAL);
    }

    @Override
    public void registerItems() {
        registerItem(new SItemBlock(SBlocks.LOG_MAGICAL_REGULAR), Names.LOG_MAGICAL_REGULAR);
        registerItem(new SItemBlock(SBlocks.LOG_MAGICAL_COLORLESS), Names.LOG_MAGICAL_COLORLESS);
        registerItem(new SItemBlock(SBlocks.LOG_MAGICAL_WHITE), Names.LOG_MAGICAL_WHITE);
        registerItem(new SItemBlock(SBlocks.LOG_MAGICAL_BLUE), Names.LOG_MAGICAL_BLUE);
        registerItem(new SItemBlock(SBlocks.LOG_MAGICAL_BLACK), Names.LOG_MAGICAL_BLACK);
        registerItem(new SItemBlock(SBlocks.LOG_MAGICAL_RED), Names.LOG_MAGICAL_RED);
        registerItem(new SItemBlock(SBlocks.LOG_MAGICAL_GREEN), Names.LOG_MAGICAL_GREEN);
        registerItem(new SItemBlock(SBlocks.PLANKS_MAGICAL), Names.PLANKS_MAGICAL);
        registerItem(new SItem(), Names.STICK_MAGICAL);
    }

    @Override
    public void registerModels() {
        SBlocks.LOG_MAGICAL_REGULAR.initModel();
        SBlocks.LOG_MAGICAL_COLORLESS.initModel();
        SBlocks.LOG_MAGICAL_WHITE.initModel();
        SBlocks.LOG_MAGICAL_BLUE.initModel();
        SBlocks.LOG_MAGICAL_BLACK.initModel();
        SBlocks.LOG_MAGICAL_RED.initModel();
        SBlocks.LOG_MAGICAL_GREEN.initModel();
        SBlocks.PLANKS_MAGICAL.initModel();
        SItems.STICK_MAGICAL.initModel();
    }

    @Override
    public void registerOreDict() {
        OreDictionary.registerOre("logWood", SBlocks.LOG_MAGICAL_REGULAR);
        OreDictionary.registerOre("logWood", SBlocks.LOG_MAGICAL_COLORLESS);
        OreDictionary.registerOre("logWood", SBlocks.LOG_MAGICAL_WHITE);
        OreDictionary.registerOre("logWood", SBlocks.LOG_MAGICAL_BLUE);
        OreDictionary.registerOre("logWood", SBlocks.LOG_MAGICAL_BLACK);
        OreDictionary.registerOre("logWood", SBlocks.LOG_MAGICAL_RED);
        OreDictionary.registerOre("logWood", SBlocks.LOG_MAGICAL_GREEN);
        OreDictionary.registerOre("plankWood", SBlocks.PLANKS_MAGICAL);
        OreDictionary.registerOre("stickWood", SItems.STICK_MAGICAL);
        OreDictionary.registerOre("logMagical", SBlocks.LOG_MAGICAL_REGULAR);
        OreDictionary.registerOre("logMagical", SBlocks.LOG_MAGICAL_COLORLESS);
        OreDictionary.registerOre("logMagical", SBlocks.LOG_MAGICAL_WHITE);
        OreDictionary.registerOre("logMagical", SBlocks.LOG_MAGICAL_BLUE);
        OreDictionary.registerOre("logMagical", SBlocks.LOG_MAGICAL_BLACK);
        OreDictionary.registerOre("logMagical", SBlocks.LOG_MAGICAL_RED);
        OreDictionary.registerOre("logMagical", SBlocks.LOG_MAGICAL_GREEN);
        OreDictionary.registerOre("plankMagical", SBlocks.PLANKS_MAGICAL);
        OreDictionary.registerOre("stickMagical", SItems.STICK_MAGICAL);
    }

    @Override
    public void registerEntities() {
        /* NOOP */
    }

    @Override
    public void registerAdvancements() {
        /* NOOP */
    }
}

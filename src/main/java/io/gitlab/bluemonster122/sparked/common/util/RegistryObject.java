package io.gitlab.bluemonster122.sparked.common.util;

import io.gitlab.bluemonster122.sparked.common.reference.ModInfo;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

abstract public class RegistryObject {
    private final ResourceLocation name;

    public RegistryObject(String name) {
        this.name = new ResourceLocation(ModInfo.MOD_ID, name);
    }

    abstract public void registerBlocks();

    abstract public void registerItems();

    abstract public void registerModels();

    abstract public void registerOreDict();

    abstract public void registerEntities();

    abstract public void registerAdvancements();

    protected void registerBlock(Block block, ResourceLocation name) {
        block.setRegistryName(name);
        block.setUnlocalizedName(name.toString());
        ForgeRegistries.BLOCKS.register(block);
    }

    protected void registerItem(Item item, ResourceLocation name) {
        item.setRegistryName(name);
        item.setUnlocalizedName(name.toString());
        ForgeRegistries.ITEMS.register(item);
    }
}

package io.gitlab.bluemonster122.sparked.client.gui;

import io.gitlab.bluemonster122.sparked.client.gui.component.GuiComponent;
import net.minecraft.client.Minecraft;

public class GuiPageCrafting extends GuiComponent {
    private final GuiSpellbook parent;

    public GuiPageCrafting(GuiSpellbook parent, int left, int top, Runnable preDrawTask) {
        super(0, 0, left + 14, top + 13, 115, 153, preDrawTask);
        this.parent = parent;
    }

    @Override
    public void initElement(Minecraft mc) {
    }

    @Override
    public void drawBackground(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
    }

    @Override
    public void drawForeground(Minecraft mc, int mouseX, int mouseY) {
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        parent.openPage("main");
    }

    @Override
    public String getName() {
        return "Spellbook - Crafting";
    }
}

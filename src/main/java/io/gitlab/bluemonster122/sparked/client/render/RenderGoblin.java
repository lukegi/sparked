package io.gitlab.bluemonster122.sparked.client.render;

import io.gitlab.bluemonster122.sparked.client.model.ModelGoblin;
import io.gitlab.bluemonster122.sparked.common.entity.EntityGoblin;
import io.gitlab.bluemonster122.sparked.common.reference.ModInfo;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;

@SideOnly(Side.CLIENT)
public class RenderGoblin extends RenderLiving<EntityGoblin> {

    private static final ResourceLocation TEXTURE = new ResourceLocation(ModInfo.MOD_ID, "textures/entities/goblin.png");

    public RenderGoblin(RenderManager rendermanagerIn) {
        super(rendermanagerIn, new ModelGoblin(), 0.5f);
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    @Nullable
    @Override
    protected ResourceLocation getEntityTexture(EntityGoblin entity) {
        return TEXTURE;
    }
}

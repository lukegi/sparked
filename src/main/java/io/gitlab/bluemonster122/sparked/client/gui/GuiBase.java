package io.gitlab.bluemonster122.sparked.client.gui;

import io.gitlab.bluemonster122.sparked.client.gui.component.GuiComponent;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.Container;
import net.minecraft.util.ResourceLocation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class GuiBase extends GuiContainer {
    protected static final ResourceLocation S_ICONS = new ResourceLocation("sparked", "textures/gui/icons.png");

    protected List<GuiComponent> components = new ArrayList<>();

    public GuiBase(Container container) {
        super(container);
    }

    @Override
    public void initGui() {
        components.forEach(component -> component.initElement(mc));
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        components.forEach(comp -> comp.drawForeground(mc, mouseX, mouseY));
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        for (GuiComponent component : components) {
            component.mouseClicked(mouseX, mouseY, mouseButton);
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        drawDefaultBackground();
        components.forEach(comp -> {
            comp.preDrawTask();
            comp.drawBackground(mc, mouseX, mouseY, partialTicks);
        });
    }

    public <T extends GuiComponent> T addComponent(T component) {
        components.add(component);
        return component;
    }

    public <T extends GuiComponent> T removeComponent(T component) {
        components.remove(component);
        return component;
    }
}

package io.gitlab.bluemonster122.sparked.client.gui.component;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;

public abstract class GuiComponent extends Gui {

    private int tX;
    private int tY;
    private int pX;
    private int pY;
    private int width;
    private int height;
    private Runnable preDrawTask;

    public GuiComponent(int tX, int tY, int pX, int pY, int width, int height, Runnable preDrawTask) {
        this.tX = tX;
        this.tY = tY;
        this.pX = pX;
        this.pY = pY;
        this.width = width;
        this.height = height;
        this.preDrawTask = preDrawTask;
    }

    public abstract void initElement(Minecraft mc);

    public abstract void drawBackground(Minecraft mc, int mouseX, int mouseY, float partialTicks);

    public abstract void drawForeground(Minecraft mc, int mouseX, int mouseY);

    public abstract void mouseClicked(int mouseX, int mouseY, int mouseButton);

    public abstract String getName();

    public void preDrawTask() {
        if (preDrawTask != null)
            preDrawTask.run();
    }

    public int getXTex() {
        return tX;
    }

    public int getYTex() {
        return tY;
    }

    public int getXPos() {
        return pX;
    }

    public int getYPos() {
        return pY;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}

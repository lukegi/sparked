package io.gitlab.bluemonster122.sparked;

import io.gitlab.bluemonster122.sparked.client.gui.GuiHandler;
import io.gitlab.bluemonster122.sparked.common.multiblock.SMultiblocks;
import io.gitlab.bluemonster122.sparked.common.network.NetworkManager;
import io.gitlab.bluemonster122.sparked.common.objects.Progression;
import io.gitlab.bluemonster122.sparked.common.objects.Tools;
import io.gitlab.bluemonster122.sparked.common.objects.Wood;
import io.gitlab.bluemonster122.sparked.common.reference.ModInfo;
import io.gitlab.bluemonster122.sparked.common.util.Registry;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.items.IItemHandler;
import org.apache.logging.log4j.Logger;

@Mod(modid = ModInfo.MOD_ID, useMetadata = true)
public class Sparked {

    @CapabilityInject(IItemHandler.class)
    public static Capability<IItemHandler> ITEM_CAP = null;

    @CapabilityInject(IEnergyStorage.class)
    public static Capability<IEnergyStorage> ENERGY_CAP = null;

    @Mod.Instance(ModInfo.MOD_ID)
    public static Sparked INSTANCE;

    public static Logger logger;

    private Registry publicRegistry;

    public Sparked() {
        publicRegistry = new Registry("public_registry",
                new Wood(),
                new Tools(),
                new Progression()
        );
    }

    @Mod.EventHandler
    public void preinit(FMLPreInitializationEvent event) {
        // Setup mod logger
        logger = event.getModLog();

        publicRegistry.registerAdvancements();

        publicRegistry.registerEntities();

        SMultiblocks.populate();
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        publicRegistry.registerOres();

        NetworkManager.init();
        if (event.getSide().isClient()) {
            NetworkRegistry.INSTANCE.registerGuiHandler(INSTANCE, new GuiHandler());
        }
    }
}
